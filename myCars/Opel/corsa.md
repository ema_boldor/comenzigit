De la 13.990 €: a șasea generație a modelului Opel Corsa echipată cu motoare diesel sau pe benzină (Germania)
Motoare avansate, poziție de ședere joasă, manevrabilitate sportivă
Eficient: greutate redusă, consum mai mic de carburant, manevrabilitate ridicată
GS Line sportiv, Corsa Elegance confortabil
Faruri adaptive IntelliLux LED® și scaune cu funcție de masaj
Inovații pentru toată lumea:  transmisia automată cu opt trepte de viteză și sisteme de asistență avansate
